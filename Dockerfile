FROM hashicorp/terraform:1.6

ADD https://github.com/terraform-linters/tflint/releases/latest/download/tflint_linux_amd64.zip /usr/local/bin/tflint.zip
RUN unzip /usr/local/bin/tflint.zip -d /usr/local/bin && \
    rm /usr/local/bin/tflint.zip
ENV PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
