# otus_iac_gitlab

## 1. Настроим terraform

Настроим хранения состояния terraform на gitlab

```bash
cd terraform
```


Чтобы не было проблем с блокировкой hashicorp, будем использовать .terraformrc:

```
provider_installation {
  network_mirror {
    url = "https://terraform-mirror.yandexcloud.net/"
    include = ["registry.terraform.io/*/*"]
  }
  direct {
    exclude = ["registry.terraform.io/*/*"]
  }
}
```

Необходимо расположить данный файл в домашней папке пользователя.

```bash
cp .terraformrc ~
```

Создадим backend.tf (файл необходимый для создания состояния tfstate)



```tf
terraform {
  backend "http" {
  }
}
```

Создадим token (YOUR-ACCESS-TOKEN) для api в gitlab (https://gitlab.com/-/user_settings/personal_access_tokens) и выдадим права для данного token

Скопируем скрипт для иницилизации tfstate <https://gitlab.com/otus_iac/dz_n5/-/terraform> и выполним его

```bash
export GITLAB_ACCESS_TOKEN=<YOUR-ACCESS-TOKEN>
export TF_STATE_NAME=iacstate
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/54265298/terraform/state/$TF_STATE_NAME" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/54265298/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/54265298/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="username=timofeevms@mail.ru" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```


Создадим необходимые переменные для terraform для gitlab:

Settings -> CI/CD -> Variables

Посмотретть переменные yandec cloud:

```bash
yc config list
```

Преобразовать ssh ключ в base64

```
cat ~/.ssh/ID_RSA | base64
```



- TF_HTTP_PASSWORD (Protected,Masked,Expanded)
- ID_RSA_PUB (Protected, Expanded )
- ID_RSA (Protected, Expanded )
- yc_cloud (Protected,Masked,Expanded)
- yc_folder (Protected,Masked,Expanded)
- yc_token (Protected,Masked,Expanded)
- db_password (Protected,Masked,Expanded)


## 2. Разработка пайплайна для создания инфраструктуры

Чтобы создать пайплайн, расположим файл с именем .gitlab-ci.yml в корне репозитория. Содержимое файла будет следующим:

```tf
stages:   
    - check
    - deploy_infra
    - destroy

.terraform_prepare: &tf_setup 
    before_script:
        - mkdir ~/.ssh/
        - echo $ID_RSA_PUB > ~/.ssh/id_rsa_testya.pub
        - cd terraform
        - terraform init     

variables:
    TF_VAR_yc_token: $yc_token
    TF_VAR_yc_cloud: $yc_cloud
    TF_VAR_yc_folder: $yc_folder
    TF_VAR_db_password: $db_password

Check terraform file:
    stage: check
    image: mrgreyves/terraform:0.3
    <<:  *tf_setup
    script:        
        - tflint
        - terraform validate
        - terraform plan
    only: 
        - main

Deploy infrustructure:
    stage: deploy_infra
    image: mrgreyves/terraform:0.3
    <<:  *tf_setup
    script:
        - terraform apply --auto-approve
    when: manual        
    only: 
        - main       

Destroy infrustructure:
    stage: destroy
    image: mrgreyves/terraform:0.3
    <<:  *tf_setup
    script:        
        - terraform destroy --auto-approve
    when: manual
    only: 
        - main
```

Основная проблема данного пайплайна это использование `image: mrgreyves/terraform:0.3`, который содержит старую версию terraform,  
не может корректно работать с yandex облаком в частности стадия terraform validate:

```bash
$ terraform validate
Error: Could not load plugin
Plugin reinitialization required. Please run "terraform init".
Plugins are external binaries that Terraform uses to access and manipulate
resources. The configuration provided requires plugins which can't be located,
don't satisfy the version constraints, or are otherwise incompatible.
Terraform automatically discovers provider requirements from your
configuration, including providers used in child modules. To see the
requirements and constraints, run "terraform providers".
Failed to instantiate provider "registry.terraform.io/yandex-cloud/yandex" to
obtain schema: Incompatible API version with plugin. Plugin version: 6, Client
versions: [5]
```

Чтобы исправить ситуацию будем использовать официальный образ hashicorp/terraform:1.6, добавим в этот образ tflint и разместим данный образ на dockerhub.
Создадим Dockerfile:

```Dockerfile
FROM hashicorp/terraform:1.6

ADD https://github.com/terraform-linters/tflint/releases/latest/download/tflint_linux_amd64.zip /usr/local/bin/tflint.zip
RUN unzip /usr/local/bin/tflint.zip -d /usr/local/bin && \
    rm /usr/local/bin/tflint.zip
ENV PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
```

Соберем новый образ и загрузим его на dockerhub.

```
docker build -t tolik1717/terraform:1.6 .
docker login
docker push tolik1717/terraform:1.6
```

Изменим .gitlab-ci.yml

```yml
stages:   
    - check
    - deploy_infra
    - destroy

.terraform_prepare: &tf_setup 
    before_script:
        - mkdir ~/.ssh/
        - echo $ID_RSA_PUB > ~/.ssh/id_rsa_testya.pub
        - ls -la
        - cd terraform
        - terraform init  

variables:
    TF_VAR_yc_token: $yc_token
    TF_VAR_yc_cloud: $yc_cloud
    TF_VAR_yc_folder: $yc_folder
    TF_VAR_db_password: $db_password

Check terraform file:
    stage: check
    image:
      name: tolik1717/terraform:1.6
      entrypoint:
        - '/usr/bin/env'
        - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
        
    <<:  *tf_setup
    script:        
        - tflint
        - terraform validate
        - terraform plan
    only: 
        - main

Deploy infrustructure:
    stage: deploy_infra
    image:
      name: tolik1717/terraform:1.6
      entrypoint:
        - '/usr/bin/env'
        - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
    <<:  *tf_setup
    script:
        - terraform apply --auto-approve
    when: manual        
    only: 
        - main       

Destroy infrustructure:
    stage: destroy
    image:
      name: tolik1717/terraform:1.6
      entrypoint:
        - '/usr/bin/env'
        - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
    <<:  *tf_setup
    script:        
        - terraform destroy --auto-approve
    when: manual
    only: 
        - main
```

Выполняем git push
Проверим состояние pipeline build по ссылке <https://gitlab.com/otus_iac/dz_n5/-/pipelines>
Проверим состояние tfstate <https://gitlab.com/otus_iac/dz_n5/-/terraform>

## 3. Установка приложения ansible

Запустим создание инфраструктуры с сайта gitlab <https://gitlab.com/otus_iac/dz_n5/-/pipelines>

![Alt text](images/image2.png)

Cкорректируем IP хостов в инвентори Ansible - ansible/environments/prod/inventory
Ip адреса созданных хостов посмотрим в стадии check pipeline gitlab.

![Alt text](images/image3.png)
![Alt text](image.png)

```txt
[wp_app]
app ansible_host=84.252.131.216
app2 ansible_host=158.160.79.176
```

Кроме этого, нам нужно поменять FQDN для базы данных в файле ansible/environments/prod/group_vars/wp_app (переменная wordpress_db_host)

```txt
wordpress_db_host: c-c9qaj0lq3k7pg44t0sfn.rw.mdb.yandexcloud.net
```

После этого добавим шаг запуска плейбука ансибл в пайплайна.

Сначала добавим новую стадию - deploy_app:

```yml
stages:
    - check
    - deploy_infra
    - deploy_app
    - destroy

Добавим еще один якорь (не то чтобы он был тут сильно нужен, но если потом добавятся дополнительные шаги с ansible-ом, то может пригодиться). Разместим его сразу после блока с .terraform_prepare

.ansible: &ansible
  stage: deploy_app
  when: manual
  image: soaron/ansible:2.9
  before_script:
    - mkdir ~/.ssh/
    - echo "$ID_RSA" | base64 -d > ~/.ssh/id_rsa_testya
    - chmod -R 700 ~/.ssh    
    - cd ansible
    - chmod o-w .

И в конец пайплайна допишем шаг:

Install application:
    stage: deploy_app
    <<: *ansible
    script:
        - ansible --version
        - ansible-playbook playbooks/install.yml
    when: manual        
    only:
        - main
```

Выполним git push и запустим пайп по установке приложения.

Wordpress

![Alt text](images/wordpress.png)

В конце запустим destroy стадию.

Ссылка на финальный Pipeline

<https://gitlab.com/otus_iac/dz_n5/-/pipelines/1153151490>

![Alt text](images/image4.png)
